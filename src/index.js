/*
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';

ReactDOM.render(<App />, document.getElementById('root'));

serviceWorker.unregister();
*/

import React from 'react'

import { render } from 'react-dom'
import { createStore } from 'redux'
import { Provider } from 'react-redux'

import App from './App.js'
import todoApp from './reducers/reducers'

let store = createStore(todoApp)
let rootElement = document.getElementById('root')

render(
   <Provider store = {store}>
      <App />
   </Provider>,
	
   rootElement
)
