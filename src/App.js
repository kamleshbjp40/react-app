import React, { Component } from 'react'
import { connect } from 'react-redux'
import { addTodo,passDatas } from './actions/actions'

import AddTodo from './components/AddTodo.js'
import TodoList from './components/TodoList.js'

class App extends Component {
	constructor(props){
		super(props);		
		this.passData = this.passData.bind(this);
	}
	passData(){
		this.props.dispatch(passDatas("kamlesh"))
	}
   render() {
      const { dispatch, visibleTodos } = this.props
      
      return (
         <div>
            <AddTodo onAddClick = {text =>dispatch(addTodo(text))} />
            <TodoList todos = {visibleTodos}/>
            
            <button onClick={this.passData}>Click Me</button>
         </div>
      )
   }
}
function select(state) {
   return {
      visibleTodos: state.todos
   }
}
export default connect(select)(App);
