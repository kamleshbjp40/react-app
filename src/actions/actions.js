export const ADD_TODO = 'ADD_TODO'
export const PASS_DATA = 'PASS_DATA'

let nextTodoId = 0;

export function addTodo(text) {
   return {
      type: ADD_TODO,
      id: nextTodoId++,
      text
   };
}

export function passDatas(text){
	
	return {
      type: PASS_DATA,
      id: nextTodoId++,
      text
   };
}
