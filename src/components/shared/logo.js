import React, { Component } from 'react';
//import logo from '../../logo.svg';
import '../../App.css';

function Clock(){
	return (
		<div>
			<h1>Hello,world</h1>
			{ new Date().toLocaleTimeString() }
		</div>
	);
}	
setInterval(Clock,1000);
class Logo extends Component {
	constructor(props) {
    super(props);
    this.state = {isToggleOn: true};

    // This binding is necessary to make `this` work in the callback
    //this.handleClick = this.handleClick.bind(this);
  }
	
	componentDidMount() {
    
  }
  render() {
	  function handleClick(e) {
		e.preventDefault();
		alert('The link was clicked.');
	  }
    return (  
      <div>
     
          <Clock />
          <a href="#" onClick={handleClick}>
      Click me
    </a>
      </div>
    );
  }
}

export default Logo;
